<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ancresdouces?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ancresdouces_description' => 'Questo plug-in consente uno scorrimento continuo della pagina quando il visitatore fa clic su un collegamento che punta a un’ancora dell’articolo da leggere.',
	'ancresdouces_nom' => 'Ancres Douces',
	'ancresdouces_slogan' => 'Collegamenti mobidi'
);
