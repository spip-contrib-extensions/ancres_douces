<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ancresdouces?lang_cible=ja
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ancresdouces_description' => 'このプラグインは、訪問者が読まれている記事のアンカーを指し示すリンクをクリックすると、ページの連続的なスクロールを可能にする。',
	'ancresdouces_nom' => '柔和なアンカー',
	'ancresdouces_slogan' => '柔和なアンカー'
);
