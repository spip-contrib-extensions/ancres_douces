<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-ancresdouces?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ancresdouces_description' => 'Ce plugin permet un défilement continu de la page lorsque le visiteur clique sur un lien pointant vers une ancre de l’article en cours de lecture.',
	'ancresdouces_nom' => 'Ancres Douces',
	'ancresdouces_slogan' => 'Des ancres en douceur'
);
